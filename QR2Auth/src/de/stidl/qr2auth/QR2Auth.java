package de.stidl.qr2auth;

/*  
 * Copyright (C) 2014-2015 Manuel Stidl, all rights reserved.
 * 
 * This file is a part of QR2Auth.
 * 
 * QR2Auth is free software; you can redistribute it and/or modify it under the
 * terms of the MIT licensee. For further information see LICENSE.txt in the
 * parent folder.
 */

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.Locale;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import android.util.Base64;
import android.util.Log;

/**
 * 
 * This class is the heart of QR2Auth. It decodes the scanned QR-Code for
 * initialization and proofs if everything was received correctly. It also
 * encodes the key for storage and the one-time password (HMAC-SHA-512) is
 * generated and returned as a hexadecimal string.
 * 
 * @author Manuel Stidl
 */

// TODO: review is required to make sure that there are no security issues

public class QR2Auth {

	final String TAG = "QR2Auth";

	/**
	 * Generates the HMAC-SHA-512 hash from the key and the contents.
	 * 
	 * @param contents
	 *            challenge if called for generating OTP - or key if called from
	 *            decryptScan() for proofing
	 * @param key
	 *            key as shared secret
	 * @return string as hexadecimal formatted HMAC-SHA-512 out of the key and
	 *         the contents
	 */
	public String hmac(String contents, String key) {
		try {
			// Initializes Mac as SHA-512, generates a SecretKey out of the key
			// and creates then the SHA-512 value
			Mac sha512_HMAC = Mac.getInstance("HmacSHA512");
			SecretKeySpec secret_key = new SecretKeySpec(key.getBytes(),
					"HmacSHA512");
			sha512_HMAC.init(secret_key);
			// Returns the generated string as Hexadecimal

			String result = String.format("%040x", new BigInteger(1,
					(sha512_HMAC.doFinal(contents.getBytes()))));

			// Handle the formatting problem byte to hexadecimal-string and add
			// the leading (but missing) zeros
			int i = result.length();
			int i2 = contents.length();
			while (i < i2) {
				String s0 = "0";
				result = s0 + result;
				i = result.length();
			}
			Log.d(TAG, result);
			return result;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * Called by the method decryptContentsRegister() for the initialization
	 * process. First the prefix is removed then a variable for proofing the
	 * password and the encoded QR-Code is generated. Then contents is encoded
	 * by BASE64 and encoded with XOR operation and the password. Finally the
	 * proofing variable is used to check if everything worked fine and the
	 * password was entered correctly.
	 * 
	 * @param contents
	 *            encoded QR-Code as string as given by the scan application
	 * @param password
	 *            entered password by the user - belonging to the QR-Code
	 * @return contents as the key for storage
	 */
	public String decryptScan(String contents, String password) {

		// Removes prefix "{key}"
		String proof = contents;
		int endKey = contents.indexOf(",");
		contents = contents.substring(5, endKey);

		// Creates variable for proofing if password and everything was correct
		int length = proof.length();
		endKey++;
		proof = proof.substring(endKey, length);

		// Decodes contents with Base64
		byte[] contentsByte = Base64.decode(contents, Base64.DEFAULT);
		byte[] passwordByte = password.getBytes();

		// Decodes contents with XOR operation
		contents = xOROperation(contentsByte, passwordByte);

		// HMAC to check if key was scanned and encrypted correctly (if password
		// was entered correctly)
		String proof1 = hmac(contents, contents);

		if (proof.equals(proof1)) {
			// Log.d(TAG, "Init successful: " + contents);
			return contents;
		}

		return "";

	}

	/**
	 * Decodes or encrypts contents (=key) with XOR operation and password.
	 * 
	 * @param contentsByte
	 *            contents as byte
	 * 
	 * @param passwordByte
	 *            password as byte
	 * @return encrypted or decoded key
	 */
	public String xOROperation(byte[] contentsByte, byte[] passwordByte) {
		String contents = null;
		try {
			byte[] result = new byte[contentsByte.length];
			// Runs the XOR operation byte per byte for both variables
			for (int i = 0; i < contentsByte.length; i++) {
				result[i] = (byte) (contentsByte[i] ^ passwordByte[i
						% passwordByte.length]);

				contents = new String(result, "UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return contents;

	}

	/**
	 * Generates the one-time password. For this the key is decoded with the
	 * password and the method xOROperation() with the parameter key and
	 * password (both as bytes) is called. Through these steps the key is
	 * decoded and then the used part for the OTP is detected. After this the
	 * prefix of contents (=challenge) is removed and the HMAC-SHA-512 is
	 * created with contents and the encoded key. Finally the challenge has to
	 * be cut to the necessary part and the OTP is returned.
	 * 
	 * @param password
	 *            entered user's password
	 * @param contents
	 *            the encoded QR-Code
	 * @param key
	 *            encoded key from SharedPreferences
	 * @return contents alias OTP
	 */
	public String generateOTP(String password, String contents, String key) {
		// Log.d(TAG, "complete: " + contents);

		// Decodes key with XOR operation
		String keyNew = xOROperation(key.getBytes(), password.getBytes());
		Log.d(TAG, "key: " + keyNew);

		// Detects what part of the OTP is used

		int start = contents.indexOf("{") + 1;
		int end = contents.indexOf(",");

		// Handle if QR-Code was not for QR2Auth
		if ((start == 0) || (end == -1)) {
			return "";
		}

		int startOTP = Integer.valueOf(contents.substring(start, end));

		start = contents.indexOf(",") + 1;
		end = contents.indexOf("}");
		int endOTP = Integer.valueOf(contents.substring(start, end));
		end++;

		// Cuts challenge
		contents = contents.substring(end, contents.length());
		// Log.d(TAG, "challenge: " + contents);

		// Generates HMAC with the scanned challenge and the decoded key
		contents = hmac(contents, keyNew);
		// Log.d(TAG, "hmac: " + contents);

		// Checks if the start of the range is higher than the end and trims
		// to the defined range
		if (startOTP < endOTP) {
			contents = contents.substring(startOTP, endOTP).toUpperCase(
					Locale.getDefault());
			// Log.d(TAG, "otp: " + contents);
		}

		// Otherwise this case will handle the range as a mathematical ring
		else {
			int e1 = contents.length();
			String s1 = contents.substring(startOTP, e1).toUpperCase(
					Locale.getDefault());
			String s2 = contents.substring(0, endOTP).toUpperCase(
					Locale.getDefault());
			contents = s1 + s2;
		}

		// Returns the OTP
		return contents;
	}
}
