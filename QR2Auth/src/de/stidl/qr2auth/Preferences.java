package de.stidl.qr2auth;

/*  
 * Copyright (C) 2014-2015 Manuel Stidl, all rights reserved.
 * 
 * This file is a part of QR2Auth.
 * 
 * QR2Auth is free software; you can redistribute it and/or modify it under the
 * terms of the MIT licensee. For further information see LICENSE.txt in the
 * parent folder.
 */

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * This class provides a fast way to handle SharedPreferences. In this
 * application the SharedPreferences are used to save and receive the key
 * (shared secret).
 * 
 * @author Manuel Stidl
 */

public class Preferences {

	private static SharedPreferences preferences;
	private static SharedPreferences.Editor editor;
	private static final String PROPERTY_KEY = "KEY";

	/**
	 * Initializes the PreferenceManager.
	 * 
	 * @param ctx
	 *            context of the activity using the SharedPreferences
	 *            (MainActivity)
	 */
	@SuppressLint("CommitPrefEdits")
	public static void init(Context ctx) {
		preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
		editor = preferences.edit();
	}

	/**
	 * Returns the stored key.
	 * 
	 * @return user's key
	 */
	public static String getKey() {
		return preferences.getString(PROPERTY_KEY, "");
	}

	/**
	 * Saves the key in the SharedPreferences.
	 * 
	 * @param key
	 *            decoded key with XOR operation.
	 */
	public static void setKey(String key) {
		editor.putString(PROPERTY_KEY, key);
		editor.commit();
	}

}