package de.stidl.qr2auth;

/*  
 * Copyright (C) 2014-2015 Manuel Stidl, all rights reserved.
 * 
 * This file is a part of QR2Auth.
 * 
 * QR2Auth is free software; you can redistribute it and/or modify it under the
 * terms of the MIT licensee. For further information see LICENSE.txt in the
 * parent folder.
 */

import qrcode.IntentIntegrator;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import de.stidl.qrtoauth.R;

/**
 * The class MainActivity of QR2Auth uses intents for scanning QR-codes with the
 * help of another application (normally XZing Barcode Scanner) which returns
 * the results. The scanned result will be delivered to the class QR2Auth for
 * decoding and generating the one-time passwords.
 * 
 * @author Manuel Stidl
 * 
 * @see qrcode.IntentIntegrator
 */

@SuppressLint("ShowToast")
public class MainActivity extends ActionBarActivity {

	private static final String TAG = "MainActivity";
	TextView textViewRegister, textViewQrCodeResult;
	Button button;
	QR2Auth qrToAuth = new QR2Auth();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		/*
		 * Initializes the Preferences, checks how big the display is and allows
		 * landscape mode or not. Then proofs if the user has already
		 * initialized and saved a key in the application as well as loading the
		 * belonging layout.
		 */

		Preferences.init(getApplicationContext());
		if (getResources().getBoolean(R.bool.portrait_only)) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}
		if (Preferences.getKey().isEmpty()) {
			setContentView(R.layout.activity_main_not_registered);
			button = (Button) findViewById(R.id.register);
			button.setOnClickListener(scanQrHandler);
			textViewRegister = (TextView) findViewById(R.id.textViewRegistered);
		} else {
			setContentView(R.layout.activity_main);
			button = (Button) findViewById(R.id.buttonScan);
			button.setOnClickListener(scanQrHandler);
			textViewQrCodeResult = (TextView) findViewById(R.id.textViewQrCodeScanResult);
			textViewRegister = (TextView) findViewById(R.id.textViewRegistered);
		}
		// Sets logo and background-color for the ActionBar
		ActionBar ab = getSupportActionBar();
		ab.setTitle(Html.fromHtml("<font color='#FFFFFF'>"
				+ getString(R.string.app_name) + "</font>"));
		ab.setDisplayShowHomeEnabled(true);
		ab.setIcon(R.drawable.ic_launcher);
		ab.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#4D4D4D")));
	}

	// Intent to scan QR-Code
	View.OnClickListener scanQrHandler = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			Log.d("Intent", "intent started");
			IntentIntegrator integrator = new IntentIntegrator(
					MainActivity.this);
			// Scan only QR codes
			integrator.initiateScan(IntentIntegrator.QR_CODE_TYPES);
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		/*
		 * Checks if the user has saved a key in the application and loads the
		 * belonging menu.
		 */
		if (Preferences.getKey().isEmpty()) {
			getMenuInflater().inflate(R.menu.not_registered_action, menu);
			Log.d("Main", "not_registered");
		} else {
			getMenuInflater().inflate(R.menu.main_action, menu);
			Log.d("Main", "registered");
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handles if the user presses delete in the menu
		int id = item.getItemId();
		if (id == R.id.action_delete) {
			deleteUser();
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * After the QR-Code was scanned and the result was given back by the
	 * scanning application. This method checks for success and calls the
	 * methods for storing or generating.
	 * 
	 * @param requestCode
	 *            is set to "0" by class IntentIntegrator.java, if it's not zero
	 *            the scan process was canceled.
	 * 
	 * @param resultCode
	 *            has to be returned as "-1" which means RESULT_OK or it failed.
	 * 
	 * @param intent
	 *            in the extras of the returned intent is the desired encoded
	 *            QR-Code stored.
	 */
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == 0) {
			if (resultCode == RESULT_OK) {
				Log.d("RESULT", "OK");
				String contents = intent.getStringExtra("SCAN_RESULT");
				int length = contents.length();

				if (Preferences.getKey().isEmpty()) {
					// Log.d("TAG", "Register");
					if ((length > 0) && (contents.contains("key"))) {
						// Shows dialog to get the decoding key with the server
						// given password
						showDialogRegisterDecrypt(contents);
					} else {
						textViewRegister
								.setText(getString(R.string.scan_fail_reg));
						for (int i = 0; i < 2; i++)
							Toast.makeText(getApplicationContext(),
									getString(R.string.toast_scan_fail_reg),
									Toast.LENGTH_LONG).show();
					}
				} else {
					if ((length > 0) && ((contents.indexOf(",") < 10)))
						// Shows dialog to get the user's password and creates
						// the OTP
						showDialogScan(contents, length);
					else
						textViewRegister
								.setText(getString(R.string.scan_fail_log));
				}
			} else if (resultCode == RESULT_CANCELED)
				Toast.makeText(getApplicationContext(),
						getString(R.string.toast_scan_canceled),
						Toast.LENGTH_LONG).show();
		}
	}

	/**
	 * The activity has to be "refreshed" to load the correct layout, especially
	 * after registration it is necessary.
	 */
	protected void refreshActivity() {
		Intent intent = new Intent(getApplicationContext(), MainActivity.class);
		startActivity(intent);
		finish();
	}

	/**
	 * ShowDialogRegisterDecrypt is the first dialog in the registration
	 * process. It asks for the password provided by the server with an
	 * AlertDialog and sends the entered password as well as the contents to the
	 * method decryptContentsRegister().
	 * 
	 * @param contents
	 *            encoded QR-Code provided by scanning application
	 */
	public void showDialogRegisterDecrypt(final String contents) {
		final EditText textEdit = new EditText(this);

		// Treats entered text as password --> not showing the whole password,
		// staring first letter lower-case
		textEdit.setInputType(InputType.TYPE_CLASS_TEXT
				| InputType.TYPE_TEXT_VARIATION_PASSWORD
				| android.text.InputType.TYPE_TEXT_FLAG_MULTI_LINE);

		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle(getString(R.string.dialog_register_encrypt_title));
		alert.setView(textEdit);
		alert.setMessage(getString(R.string.dialog_register_encrypt_message));
		alert.setIcon(android.R.drawable.ic_dialog_alert);

		alert.setPositiveButton(getString(R.string.dialog_yes),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						String password = textEdit.getText().toString()
								.toLowerCase();
						// Calls method for decoding and registration
						decryptContentsRegister(password, contents);
					}
				});

		alert.setNegativeButton(getString(R.string.dialog_no),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
					}
				});

		AlertDialog dialog = alert.create();
		dialog.setOnShowListener(new OnShowListener() {

			// Shows keyboard when dialog is opened
			@Override
			public void onShow(DialogInterface dialog) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.showSoftInput(textEdit, InputMethodManager.SHOW_IMPLICIT);
			}
		});

		dialog.show();
	}

	/**
	 * Asks for the user's password for encoding with an AlertDialog and sends
	 * the entered password as well as the contents to the method
	 * setContentsRegister().
	 * 
	 * @param contents
	 *            encoded and adjusted QR-Code by QR2Auth = key
	 */
	public void showDialogRegisterStorePw(final String contents) {
		final EditText enteredPassword = new EditText(this);
		final EditText enteredPassword2 = new EditText(this);

		// // TODO: fails on Android 4.1.2? Also the first letter is displayed
		// // there as a star
		// // Treats entered text as password --> not showing the whole password
		enteredPassword.setInputType(InputType.TYPE_CLASS_TEXT
				| InputType.TYPE_TEXT_VARIATION_PASSWORD);
		enteredPassword2.setInputType(InputType.TYPE_CLASS_TEXT
				| InputType.TYPE_TEXT_VARIATION_PASSWORD);

		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle(getString(R.string.dialog_register_title));
		alert.setMessage(getString(R.string.dialog_register_message));
		alert.setIcon(android.R.drawable.ic_dialog_alert);

		// TODO: verena gegenlesen!
		enteredPassword.setHint(getString(R.string.dialog_password_hint1));
		enteredPassword2.setHint(getString(R.string.dialog_password_hint2));
		LinearLayout layout = new LinearLayout(getApplicationContext());
		layout.setOrientation(LinearLayout.VERTICAL);
		layout.addView(enteredPassword);
		layout.addView(enteredPassword2);
		alert.setView(layout);

		alert.setPositiveButton(getString(R.string.dialog_yes),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						String password = enteredPassword.getText().toString();
						String password2 = enteredPassword2.getText()
								.toString();
						if ((password.equals(password2) && (checkPw(password))))
							setContentsRegister(password, contents);
						else {
							for (int i = 0; i < 3; i++) {
								Toast.makeText(getApplicationContext(),
										getString(R.string.toast_pw_wrong),
										Toast.LENGTH_SHORT).show();
							}
							showDialogRegisterStorePw(contents);
						}
					}
				});

		alert.setNegativeButton(getString(R.string.dialog_no),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
					}
				});

		AlertDialog dialog = alert.create();
		dialog.setOnShowListener(new OnShowListener() {

			// Shows keyboard when dialog is opened
			@Override
			public void onShow(DialogInterface dialog) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.showSoftInput(enteredPassword,
						InputMethodManager.SHOW_IMPLICIT);
			}
		});

		dialog.show();

	}

	/**
	 * Opens a dialog and asks for the user's password. Calls the method
	 * setContentsScan().
	 * 
	 * @param contents
	 *            encoded QR-Code returned by the scanning application
	 * @param length
	 *            length of contents
	 */
	public void showDialogScan(final String contents, final int length) {
		final EditText textEdit = new EditText(this);
		// Treats entered text as password --> not showing the whole password,
		// using stars instead
		textEdit.setInputType(InputType.TYPE_CLASS_TEXT
				| InputType.TYPE_TEXT_VARIATION_PASSWORD);

		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle(getString(R.string.dialog_scan_title));
		alert.setView(textEdit);
		alert.setMessage(getString(R.string.dialog_scan_message));
		alert.setIcon(android.R.drawable.ic_dialog_alert);

		alert.setPositiveButton(getString(R.string.dialog_yes),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						String password = textEdit.getText().toString();
						setContentsScan(password, contents);
					}
				});

		alert.setNegativeButton(getString(R.string.dialog_no),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
					}
				});

		AlertDialog dialog = alert.create();
		dialog.setOnShowListener(new OnShowListener() {

			// Shows keyboard when dialog is shown
			@Override
			public void onShow(DialogInterface dialog) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.showSoftInput(textEdit, InputMethodManager.SHOW_IMPLICIT);
			}
		});

		dialog.show();

	}

	/**
	 * Loads the encrypted stored key from SharedPreferences and sends the
	 * password, the contents and the encrypted key to the method generateOTP().
	 * 
	 * @param password
	 *            entered user's password for decoding the stored key
	 * @param contents
	 *            encoded QR-Code returned by the scanning application and
	 *            forwarded by calling method setContentsScan()
	 */
	protected void setContentsScan(String password, String contents) {
		String key = Preferences.getKey();
		textViewQrCodeResult.setText(qrToAuth.generateOTP(password, contents,
				key));
		contents = (String) textViewQrCodeResult.getText();
		if (contents.contains("Something") || contents.isEmpty())
			textViewRegister.setText(getString(R.string.try_again));
		else
			textViewRegister.setText(getString(R.string.generated));
	}

	/**
	 * Decodes the scanned key and calls the second dialog
	 * showDialogRegisterStorePw(contents) for the user's password if the
	 * encoding was successful (see {@code password}). Otherwise it makes a
	 * Toast saying that the password was entered wrong.
	 * 
	 * @param password
	 *            the password provided by the server
	 * 
	 * @param contents
	 *            the whole encoded QR-Code like it was returned by third party
	 *            application
	 */
	protected void decryptContentsRegister(String password, String contents) {

		// Decodes key
		contents = qrToAuth.decryptScan(contents, password);
		// Calls second Dialog to ask for storage password
		if (contents != "")
			showDialogRegisterStorePw(contents);

		else {
			textViewRegister.setText(getString(R.string.not_registered));
			// TODO: find other solution
			// "hacky" way to display Toast longer
			for (int i = 0; i < 3; i++)
				Toast.makeText(getApplicationContext(),
						getString(R.string.toast_reg_fail), Toast.LENGTH_LONG)
						.show();

		}
	}

	/**
	 * Checks if the password fulfills requirements and saves the key encoded
	 * with the password at SharedPreferences. The key can be stored at
	 * SharedPreferences and has not to be encrypted high level in a special
	 * KeyStore, because it is a XOR-operation. There is no possibility to
	 * brute-force! For this the server has to revoke the key after little fail
	 * attempts.
	 * 
	 * @param password
	 *            the user's password for encryption
	 * @param contents
	 *            the key decoded with QR2Auth
	 */
	protected void setContentsRegister(String password, String contents) {
		// Encrypts key with password
		contents = qrToAuth.xOROperation(contents.getBytes(),
				password.getBytes());
		if (!contents.isEmpty()) {
			// Saves encrypted key
			Preferences.setKey(contents);
			Toast.makeText(getApplicationContext(),
					getString(R.string.toast_reg_succ), Toast.LENGTH_LONG)
					.show();
			refreshActivity();
		} else {
			textViewRegister.setText(getString(R.string.not_registered));
			// TODO: find other solution
			// "hacky" way to display Toast longer
			for (int i = 0; i < 3; i++)
				Toast.makeText(getApplicationContext(),
						getString(R.string.toast_reg_fail), Toast.LENGTH_LONG)
						.show();
		}
	}

	/**
	 * Proofs password for its length an checks if it contains characters and
	 * digits.
	 * 
	 * @param text
	 *            user's password for encryption.
	 * @return boolean - Does the password match requirements?
	 */
	private boolean checkPw(String text) {
		if ((text.length() >= 4) && text.matches(".*\\d.*")
				&& text.matches(".*[A-Za-z].*"))
			return true;

		else
			return false;
	}

	/**
	 * Makes sure that the user really wants to delete his key. If yes - set the
	 * key in the Preferences to an empty string.
	 */
	private void deleteUser() {
		new AlertDialog.Builder(this)
				.setTitle(getString(R.string.dialog_delete_title))
				.setMessage(getString(R.string.dialog_delete_message))
				.setPositiveButton(android.R.string.yes,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// Removes the key
								Preferences.setKey("");
								refreshActivity();
							}
						})
				.setNegativeButton(android.R.string.no,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// Do nothing
							}
						}).setIcon(android.R.drawable.ic_dialog_alert).show();
	}
}
