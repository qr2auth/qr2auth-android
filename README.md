# QR2Auth #

This repository covers only the Android application. 


### What is QR2Auth? ###
QR2Auth is a user friendly challenge response authentication protocol that uses QR codes.


### Requirements ###
* Android API level > 11
* Barcode Scanner ( https://github.com/zxing/zxing)


### QR2Auth Server Application ###
You can find the server part, a Python Django version at https://bitbucket.org/qr2auth/qr2auth-django-auth-qr2auth. There is also a demo project available.



### TODO & Issues ###
Please reports bugs to https://bitbucket.org/qr2auth/qr2auth-android/issues.

### License ###
Please see [License.txt](https://bitbucket.org/qr2auth/qr2auth-android/src/f82724186718b302b8a451b8f8b288c6325761f7/QR2Auth/LICENSE.txt?at=master).